import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

def get_eta():
    f1 = 1./12;
    a1 = 1.60;
    f2 = 1./4;
    a2 = 0.85;
    f3 = 1./1.6;
    a3 = 0.15;
    dt=0.375;
    T=12.
    pi = np.pi
    t = np.arange(0., T, dt)

    print f1, f2, f3
    return t, a1*np.sin(2*pi*f1*t) + a2*np.sin(2*pi*f2*t) + a3*np.sin(2*pi*f3*t)

def fourier_transform(eta):
    N = len(eta)
    n = np.arange(N/2, dtype=np.float)
    q = np.arange(N)

    An = np.zeros(n.shape)
    Bn = np.zeros(n.shape)

    An[0] = eta.mean()
    for i in tqdm(range(1, len(An))):
        an = eta * np.cos(2*np.pi*q*n[i]/N)
        bn = eta * np.sin(2*np.pi*q*n[i]/N)
        An[i] = 2./N * an[i:].sum()
        Bn[i] = 2./N * bn[i:].sum()
    return n, An**2 + Bn**2

def npfft(eta):
    """Numpy's FFT"""
    N = len(eta)
    dft = np.fft.fft(eta)/(N/2)
    E = np.sqrt(dft[:N/2].real**2 + dft[:N/2].imag**2)
    return np.arange(N/2, dtype=np.float), E


if __name__ == "__main__":
    t, eta = get_eta()
    n, ft = fourier_transform(eta)
    freq = n/t[-1]

    plt.plot(t, eta)
    plt.show()

    plt.plot(freq, ft)
    plt.show()



