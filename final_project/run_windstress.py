from shallow_water import create_anim
from rotate_sw import WindStressSW as SW
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from netCDF4 import Dataset
import tqdm


Lx = 4e7
Ly = 4e7
dx = 4e5
dy = 4e5
steps = 50000
ival = 20

#sw = SW(dx, dy, Lx, Ly, quiver=True)
#print "Time step:", sw.dt
#print "Sim time: ", sw.dt*steps
#print "Fric time:", 1/sw.kappa.mean()
#print "Rossby vel:",sw.cR.mean()
#print "Rossby time:", Lx / sw.cR.mean()
#print "Rossby R: ", sw.RR.mean()
#print "WBC scale:", sw.wbcthickness, '(',sw.wbcthickness/Lx,')'
#print "Gravity wave ", sw.c0
#print "Grid    : ", dx, dy
#
#sw.simulate(steps)
#scale = sw.eta.max()
#scale = 0.02
#
#print 'loading...'
with Dataset("present.nc", "r") as df:
#with Dataset("longrun.nc", "r") as df:
    ETA = df.variables['eta'][:,:,:]
    x = df.variables['x'][:]
    y = df.variables['y'][:]
print 'done loading...'
print ETA.shape

i = 5
#hm = ETA[::i, 100, :]
#time = np.arange(0., sw.dt*steps, sw.dt*i)
#time, xcoord = np.meshgrid(time, sw.x[0])
#print time.shape, xcoord.shape, hm.shape
#plt.pcolormesh(xcoord, time, hm.T)
#plt.show()


ETA = ETA[::50,:,:]

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.gca()
pbar = tqdm.tqdm(total=ETA.shape[0])

def animate(i):
    ax.clear()
    ax.set_xlabel(i)
    ax.contour(x, y, ETA[i], 20)
    pbar.update()
    return ax,

im = ax.contour(x, y, ETA[0])
ani = animation.FuncAnimation(fig, animate, ETA.shape[0], interval=20, blit=True)

plt.show()





#ax.set_xlim((x.min(), x.max()))
#ax.set_ylim((y.min(), y.max()))
#im = ax.pcolor(x, y, ETA[0], animated=True, vmin=-scale, vmax=scale)
#plt.colorbar(im)
#pbar = tqdm.tqdm(total=ETA.shape[0])
## animation function. This is called sequentially
#def animate(i):
#    pbar.update(1)
#    im.set_array(ETA[i, :-1, :-1].flatten())
#    return im,
## call the animator. blit=True means only re-draw the parts that have changed.
#anim = animation.FuncAnimation(fig, animate, frames=ETA.shape[0], interval=ival, blit=True)
#plt.show()
