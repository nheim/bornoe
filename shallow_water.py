import numpy as np
import warnings
import tqdm
from netCDF4 import Dataset


class SW2D(object):

    def __init__(self, dx, dy, Lx, Ly, H=400., quiver=False):
        self.quiver = quiver
        self.g = 0.01
        self.H = H
        self.Lx = Lx
        self.Ly = Ly
        self.dx = dx
        self.dy = dy
        self.xpts = int(Lx/dx)
        self.ypts = int(Ly/dy)
        self.u = np.zeros((self.ypts+2, self.xpts+2))
        self.v = np.zeros((self.ypts+2, self.xpts+2))
        self.eta = np.zeros((self.ypts+2,self.xpts+2))
        self.define_grid()
        self.output = max(int(3600. / self.dt), 1)

    def define_grid(self):
        # grid definition
        self.x = np.arange(0., self.Lx, self.dx, dtype=np.float)
        self.y = np.arange(-self.Ly/2., self.Ly/2., self.dy, dtype=np.float)
        self.x, self.y = np.meshgrid(self.x, self.y)
        self.y = self.y
        self.dt = self.dx / np.sqrt(self.g*self.H) / 4.
        # init arrays
        xp = yp = self.Lx/4.
        self.eta[1:-1, 1:-1] = self.set_gaussian(xp, yp, 5e4)

    def set_gaussian(self, xp, yp, width):
        return np.exp( - ((self.x - xp)**2 + (self.y - yp)**2)/(width)**2)

    def pad_edge(self, var):
        var[0, :] = var[1, :]
        var[-1, :] = var[-2, :]
        var[:, 0] = var[:, 1]
        var[:, -1] = var[:, -2]

    def pad_const(self, var, const=0.):
        var[0, :] = const
        var[-1, :] = const
        var[:, 0] = const
        var[:, -1] = const

    def step_velocities(self):
        u, v = self.u, self.v
        eta = self.eta
        txg = self.dt/self.dx * self.g
        tyg = self.dt/self.dy * self.g
        self.pad_edge(eta)
        u[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1])
        v[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1])

    def do_step(self):
        u, v = self.u, self.v
        eta = self.eta
        # step velocities
        self.step_velocities()
        # prevent flux through boundary
        self.pad_const(u)
        self.pad_const(v)
        # update height related variables
        self.step_eta()

    def step_eta(self):
        u, v, eta = self.u, self.v, self.eta
        h = eta + self.H
        ujk = u[1:-1, 1:-1]; u1k = u[1:-1, :-2]
        vjk = v[1:-1, 1:-1]; v1k = v[:-2, 1:-1]
        fe = np.where(ujk>0, ujk*h[1:-1, 1:-1], ujk*h[1:-1, 2:])
        fw = np.where(u1k>0, u1k*h[1:-1, :-2], u1k*h[1:-1, 1:-1])
        fn = np.where(vjk>0, vjk*h[1:-1, 1:-1], vjk*h[2:, 1:-1])
        fs = np.where(v1k>0, v1k*h[:-2, 1:-1], v1k*h[1:-1, 1:-1])
        # step eta
        eta[1:-1, 1:-1] -= self.dt/self.dx * (fe - fw) + self.dt/self.dy * (fn - fs)

    def check_mass_conservation(self):
        m = self.eta[1:-1, 1:-1].sum()
        if np.abs(1. - self.m/m) > 1e-8:
            warnings.warn("Mass is not conserved! Before {}, After {}".format(self.m, m))

    def smooth(self):
        eta = self.eta
        eps = 0.1
        eta[1:-1, 1:-1] = (1-eps)*eta[1:-1, 1:-1] + eps/4. * \
                (eta[:-2, 1:-1]+eta[2:, 1:-1]+eta[1:-1, :-2]+eta[1:-1, 2:])
        eta = np.pad(eta[1:-1, 1:-1], 1, 'reflect')
        self.eta = eta

    def simulate(self, steps, quiver=False):
        self.create_outputfile()
        self.df.setncattr('steps', steps)
        output = self.output
        self.m = self.eta.sum()
        for i in tqdm.tqdm(range(steps)):
            self.do_step()
            if i%output == 0:
                self.checkpoint(i//output)
        self.check_mass_conservation()
        self.df.close()

    def create_filename(self):
        return '{}_L{}_dx{}.nc'.format(self.__class__.__name__, self.Lx, self.dx)

    def checkpoint(self, i):
        self.nceta[i, :, :] = self.eta[1:-1, 1:-1]
        if self.quiver:
            self.ncu[i, :, :] = self.u[1:-1, 1:-1]
            self.ncv[i, :, :] = self.v[1:-1, 1:-1]

    def setncattributes(self):
        self.df.setncattr('simulated_timestep', self.dt)
        self.df.setncattr('saved_timestep', self.output*self.dt)
        self.df.setncattr('Lx', self.Lx)
        self.df.setncattr('Ly', self.Ly)
        self.df.setncattr('dy', self.dy)
        self.df.setncattr('dx', self.dx)
        self.df.setncattr('H', self.H)
        self.df.setncattr('g', self.g)


    def create_outputfile(self):
        self.fname = self.create_filename()
        self.df = Dataset(self.fname, 'w')
        self.setncattributes()
        xdim = self.df.createDimension('x', self.xpts)
        ydim = self.df.createDimension('y', self.ypts)
        tdim = self.df.createDimension('time', None)

        x = self.df.createVariable('x', np.float, ('y', 'x'))
        y = self.df.createVariable('y', np.float, ('y', 'x'))
        x[:,:] = self.x[:, :]
        y[:,:] = self.y[:, :]
        self.nceta = self.df.createVariable('eta', np.float, ('time', 'y', 'x'))
        if self.quiver:
            self.ncu = self.df.createVariable('uvel', np.float, ('time', 'y', 'x'))
            self.ncv = self.df.createVariable('vvel', np.float, ('time', 'y', 'x'))

    def print_info(self):
        print "Height:       ", self.H
        print "Basin widths: ", self.Lx, self.Ly
        print "Grid spacing: ", self.dx, "x", self.dy
        print "Grid size:    ", self.eta.shape

class SW2DPeriodic(SW2D):
    def __init__(self, dx, dy, Lx, Ly):
        super(SW2DPeriodic, self).__init__(dx, dy, Lx, Ly)

    def pad_wrap(self, var):
        var[0, :] = var[-2, :]
        var[-1, :] = var[1, :]
        var[:, 0] = var[:, -2]
        var[:, -1] = var[:, 1]

    def do_step(self):
        u, v = self.u, self.v
        eta = self.eta
        # step velocities
        txg = self.dt/self.dx * self.g
        tyg = self.dt/self.dy * self.g
        self.pad_wrap(eta)
        u[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1])
        v[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1])
        # prevent flux through boundary
        self.pad_wrap(u)
        self.pad_wrap(v)
        # update height related variables
        h = eta + self.H; ujk = u[1:-1, 1:-1]; u1k = u[:-2, 1:-1]
        fe = np.where(ujk>0, ujk*h[1:-1, 1:-1], ujk*h[2:, 1:-1])
        fw = np.where(u1k>0, u1k*h[:-2, 1:-1], u1k*h[1:-1, 1:-1])
        vjk = v[1:-1, 1:-1]; v1k = v[1:-1, :-2]
        fn = np.where(vjk>0, vjk*h[1:-1, 1:-1], vjk*h[1:-1, 2:])
        fs = np.where(v1k>0, v1k*h[1:-1, :-2], v1k*h[1:-1, 1:-1])
        # step eta
        eta[1:-1, 1:-1] -= self.dt/self.dx * (fe - fw) + self.dt/self.dy * (fn - fs)


def create_anim(x, y, ETA, ival=20, figsize=(8,8), scale=0.1):
    import matplotlib.pyplot as plt
    from matplotlib import animation
    # First set up the figure, the axis, and the plot element we want to animate
    fig = plt.figure()
    ax = plt.gca()
    ax.set_xlim((x.min(), x.max()))
    ax.set_ylim((y.min(), y.max()))
    im = ax.pcolor(x, y, ETA[0], animated=True, vmin=-scale, vmax=scale)
    plt.colorbar(im)
    pbar = tqdm.tqdm(total=ETA.shape[0])
    # animation function. This is called sequentially
    def animate(i):
        pbar.update(1)
        im.set_array(ETA[i, :-1, :-1].flatten())
        return im,
    # call the animator. blit=True means only re-draw the parts that have changed.
    anim = animation.FuncAnimation(fig, animate, frames=ETA.shape[0], interval=ival, blit=True)
    return anim

def quiver_anim(x, y, U, V, ival=20, i=5):
    import matplotlib.pyplot as plt
    from matplotlib import animation
    X = x[::i,::i]; Y = y[::i,::i]
    fig = plt.figure()
    ax = plt.gca()
    U *= 3; V *= 3
    im = ax.quiver(X, Y, U[0,::i,::i], V[0,::i,::i], scale=0.1)
    # animation function. This is called sequentially
    def animate(j):
        im.set_UVC(U[j,::i,::i], V[j,::i,::i])
        return im,
    # call the animator. blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate, frames=U.shape[0], interval=ival, blit=True)


