from rotate_sw import RotateSW, BottomFrictionSW
from fourier import fourier_transform, npfft
import matplotlib.pyplot as plt

Lx = Ly = 1e6
dx = dy = 5e3
steps = 10000
interval = 1

sw = BottomFrictionSW(dx, dy, Lx, Ly)

sw.eta[1:-1,1:-1] = sw.set_gaussian(Lx/2., 0., Lx/15.)
#ETA, U, V = sw.simulate(steps, output=interval, quiver=False)
sw.print_info()

eta = ETA[:, 50, 50]
n, fft = npfft(eta)
freq = n/(steps*sw.dt)
plt.plot(freq, fft)
plt.loglog()
plt.show()

# anim = sw.quiver_anim(U, V, f=ETA.shape[0])
# plt.show()
