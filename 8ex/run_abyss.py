from shallow_water import create_anim
from rotate_sw import AbyssalSW as SW
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from netCDF4 import Dataset
import tqdm


Lx = 6000e3
Ly = 6000e3
dx = Lx/100.
dy = Ly/100.
steps = 10000
ival = 20
skip = 50
i = 5
quiver = True

sw = SW(dx, dy, Lx, Ly, quiver=True)
sw.simulate(steps)

print 'loading...'
with Dataset(sw.fname, "r") as df:
#with Dataset("AbyssalSW_L6000000.0_dx30000.0.nc", "r") as df:
#with Dataset("nosink_source.nc", "r") as df:
    if quiver:
        V = df.variables['vvel'][:,:,:]
        U = df.variables['uvel'][:,:,:]
    else:
        ETA = df.variables['eta'][:]
    x = df.variables['x'][:]
    y = df.variables['y'][:]
    print df.getncattr('simulated_timestep')
    print df.getncattr('saved_timestep')
print 'done loading...'

#hm = ETA[::i, 100, :]
#time = np.arange(0., sw.dt*steps, sw.dt*i)
#time, xcoord = np.meshgrid(time, sw.x[0])
#print time.shape, xcoord.shape, hm.shape
#plt.pcolormesh(xcoord, time, hm.T)
#plt.show()



# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.gca()

if quiver:
    U = U[::skip,:,:]
    V = V[::skip,:,:]
    scale = 0.1
    im = ax.quiver(x[::i,::i], y[::i,::i], U[0, ::i,::i], V[0,::i,::i], scale=0.2)
    pbar = tqdm.tqdm(total=U.shape[0])
    # animation function. This is called sequentially
    def animate(j):
        pbar.update(1)
        im.set_UVC(U[j,::i,::i], V[j,::i,::i])
        return im,
    # call the animator. blit=True means only re-draw the parts that have changed.
    anim = animation.FuncAnimation(fig, animate, frames=U.shape[0], interval=ival, blit=True)
else:
    ETA = ETA[::skip]
    scale = ETA.max()/2.
    ax.set_xlim((x.min(), x.max()))
    ax.set_ylim((y.min(), y.max()))
    im = ax.pcolor(x, y, ETA[0], animated=True, vmin=-scale, vmax=scale)
    plt.colorbar(im)
    pbar = tqdm.tqdm(total=ETA.shape[0])
    # animation function. This is called sequentially
    def animate(i):
        pbar.update(1)
        im.set_array(ETA[i, :-1, :-1].flatten())
        return im,
    # call the animator. blit=True means only re-draw the parts that have changed.
    anim = animation.FuncAnimation(fig, animate, frames=ETA.shape[0], interval=ival, blit=True)

plt.show()
