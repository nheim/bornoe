import numpy as np
import matplotlib.pyplot as plt

from rotate_sw import WindStressSW

Lx = Ly = 1e6
dx = dy = 2e3
steps = 10000

sw = WindStressSW(dx, dy, Lx, Ly)
sw.eta[1:-1,1:-1] = sw.set_gaussian(Ly/2., Ly/2., 5e4)
# ETA = sw.simulate(steps, output=20)



