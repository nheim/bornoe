import numpy as np
from shallow_water import SW2D, SW2DPeriodic
import tqdm


class RotateSW(SW2D):
    def __init__(self, dx, dy, Lx, Ly, quiver=False):
        super(RotateSW, self).__init__(dx, dy, Lx, Ly, quiver=quiver)
        self.f = 1e-5
        self.alpha = self.dt * self.f
        self.b = self.alpha**2/4.

    def step_velocities(self):
        u, v = self.u, self.v
        uh, vh = u, v
        uo, vo = u, v
        eta = self.eta
        # step velocities
        txg = self.dt/self.dx * self.g
        tyg = self.dt/self.dy * self.g
        self.pad_edge(eta)
        # predict
        uh[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1])
        vh[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1])

        u[:, :] = (uh - self.b*uo + self.alpha*vo)/(1 + self.b)
        v[:, :] = (vh - self.b*vo - self.alpha*uo)/(1 + self.b)
        u[:, -2] = 0.
        v[-2, :] = 0.

    def print_info(self):
        R = np.sqrt(self.g * self.H)/self.f
        print "Height:       ", self.H
        print "Coriolis:     ", self.f
        print "Basin widths: ", self.Lx, self.Ly
        print "Grid spacing: ", self.dx, "x", self.dy
        print "Grid size:    ", self.eta.shape
        print "Rossby radius:", R


class BetaPlaneSW(RotateSW):
    def __init__(self, dx, dy, Lx, Ly, quiver=False):
        super(BetaPlaneSW, self).__init__(dx, dy, Lx, Ly, quiver=quiver)
        self.f0 = 1e-5
        self.beta = 1e-11
        self.f = self.f0 + self.beta*self.y
        self.f = np.pad(self.f, 1, mode='edge')
        self.alpha = self.dt * self.f
        self.b = self.alpha**2/4.

    def print_info(self):
        R = np.sqrt(self.g * self.H)/self.f
        print "Height:       ", self.H
        print "Coriolis:     ", self.f0
        print "Beta:         ", self.beta
        print "Basin widths: ", self.Lx, self.Ly
        print "Grid spacing: ", self.dx, "x", self.dy
        print "Grid size:    ", self.eta.shape
        print "Rossby radius:", self.c0/self.f0


class EQSW(RotateSW):
    def __init__(self, dx, dy, Lx, Ly, quiver=False):
        super(EQSW, self).__init__(dx, dy, Lx, Ly, quiver=quiver)
        self.R = 6371e3
        self.g = 0.01
        self.H = 400.
        self.dt = self.dx / np.sqrt(self.g*self.H) * 0.1

        self.f0 = 0.
        self.beta = 1e-11
        self.f = self.f0 + self.beta * self.y
        self.f = np.pad(self.f, 1, mode='edge')
        self.alpha = self.dt * self.f
        self.b = self.alpha**2/4.

        self.c0 = np.sqrt(self.g*self.H)
        self.cR = self.c0 / 3.
        self.tR = self.Lx / self.cR.mean()
        self.RR = np.sqrt(self.c0/(2*self.beta))


    def print_info(self):
        R = np.sqrt(self.g * self.H)/self.f
        print "Height:       ", self.H
        print "Basin widths: ", self.Lx, self.Ly
        print "Grid spacing: ", self.dx, "x", self.dy
        print "Grid size:    ", self.eta.shape
        print "Rossby radius x = y = 0:", R[0,0]
        print "c             ", np.sqrt(self.g*self.H)


class BFricEQSW(EQSW):
    def __init__(self, dx, dy, Lx, Ly, quiver=False):
        super(BFricEQSW, self).__init__(dx, dy, Lx, Ly, quiver=quiver)
        self.kappa = np.zeros(self.x.shape)
        w = 10
        self.k = 1e-4
        self.kappa[:w, :] = self.k
        self.kappa[-w-1:, :] = self.k
        self.kappa[:, :w] = self.k
        self.kappa[:, -w-1:] = self.k

    def step_velocities(self):
        u, v = self.u, self.v
        uh, vh = u, v
        uo, vo = u, v
        eta = self.eta
        # step velocities
        txg = self.dt/self.dx * self.g
        tyg = self.dt/self.dy * self.g
        self.pad_edge(eta)
        # predict
        uh[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1])
        vh[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1])
        uh[1:-1, 1:-1] /= 1 + self.dt * self.kappa
        vh[1:-1, 1:-1] /= 1 + self.dt * self.kappa

        u[:, :] = (uh - self.b*uo + self.alpha*vo)/(1 + self.b)
        v[:, :] = (vh - self.b*vo - self.alpha*uo)/(1 + self.b)
        u[:, -2] = 0.
        v[-2, :] = 0.

    def print_info(self):
        R = np.sqrt(self.g * self.H)/self.f
        self.c0 = np.sqrt(self.g * self.H)
        print "Height:       ", self.H
        print "Gravity:      ", self.g
        print "Coriolis:     ", self.f0
        print "Beta:         ", self.beta
        print "Time step:    ", self.dt
        print "Basin widths: ", self.Lx, 'x', self.Ly
        print "Grid spacing: ", self.dx, "x", self.dy
        print "Grid size:    ", self.eta.shape
        print "Rossby radius:", np.sqrt(self.c0/(2*self.beta))
        print "kappa fric:   ", self.k



class BottomFrictionSW(BetaPlaneSW):
    def __init__(self, dx, dy, Lx, Ly, quiver=False):
        super(BottomFrictionSW, self).__init__(dx, dy, Lx, Ly, quiver=quiver)
        self.g = 0.01
        self.H = 400.
        self.f0 = 2e-5
        self.beta = 0.
        self.f = self.f0 + self.beta*self.y
        self.f = np.pad(self.f, 1, mode='edge')
        self.alpha = self.dt * self.f
        self.b = self.alpha**2/4.
        self.dt = self.dx / np.sqrt(self.g*self.H) * 0.5

        self.kappa = np.zeros(self.x.shape)
        w = 10
        self.k = 1e-4
        self.kappa[:w, :] = self.k
        self.kappa[-w-1:, :] = self.k
        self.kappa[:, :w] = self.k
        self.kappa[:, -w-1:] = self.k
        #self.dt *= 0.1

    def step_velocities(self):
        u, v = self.u, self.v
        uh, vh = u, v
        uo, vo = u, v
        eta = self.eta
        # step velocities
        txg = self.dt/self.dx * self.g
        tyg = self.dt/self.dy * self.g
        self.pad_edge(eta)
        # predict
        uh[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1])
        vh[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1])
        uh[1:-1, 1:-1] /= 1 + self.dt * self.kappa
        vh[1:-1, 1:-1] /= 1 + self.dt * self.kappa

        u[:, :] = (uh - self.b*uo + self.alpha*vo)/(1 + self.b)
        v[:, :] = (vh - self.b*vo - self.alpha*uo)/(1 + self.b)
        u[:, -2] = 0.
        v[-2, :] = 0.

#    def step_velocities(self):
#        u, v = self.u, self.v
#        uh, vh = u, v
#        uo, vo = u, v
#        eta = self.eta
#        # step velocities
#        txg = self.dt/self.dx * self.g
#        tyg = self.dt/self.dy * self.g
#        self.pad_edge(eta)
#        # predict
#        uh[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1])
#        vh[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1])
#
#        b1 = (self.b[1:-1, :-2] + self.b[1:-1,1:-1])/2.
#        b2 = (self.b[:-2, 1:-1] + self.b[1:-1,1:-1])/2.
#        a1 = (self.alpha[:-2,1:-1] + self.alpha[1:-1,1:-1])/2.
#        a2 = (self.alpha[1:-1,:-2] + self.alpha[1:-1,1:-1])/2.
#        v1 = (vo[1:-1,:-2]+vo[1:-1,2:]+vo[:-2,1:-1]+vo[2:,1:-1])/4.
#        u1 = (uo[1:-1,:-2]+uo[1:-1,2:]+uo[:-2,1:-1]+uo[2:,1:-1])/4.
#        u[1:-1, 1:-1] = (uh[1:-1, 1:-1] - b1*uo[1:-1, 1:-1] + a1*v1)/(1. + b1)
#        v[1:-1, 1:-1] = (vh[1:-1, 1:-1] - b2*vo[1:-1, 1:-1] - a2*u1)/(1. + b2)
#        u[:, -2] = 0.
#        v[-2, :] = 0.

    def print_info(self):
        R = np.sqrt(self.g * self.H)/self.f
        self.c0 = np.sqrt(self.g * self.H)
        print "Height:       ", self.H
        print "Gravity:      ", self.g
        print "Coriolis:     ", self.f0
        print "Beta:         ", self.beta
        print "Time step:    ", self.dt
        print "Basin widths: ", self.Lx, 'x', self.Ly
        print "Grid spacing: ", self.dx, "x", self.dy
        print "Grid size:    ", self.eta.shape
        print "Rossby radius:", self.c0/self.f0
        print "kappa fric:   ", self.k


class WindStressSW(BetaPlaneSW):
    def __init__(self, dx, dy, Lx, Ly, quiver=False):
        super(WindStressSW, self).__init__(dx, dy, Lx, Ly, quiver=quiver)
        self.H = 4000.
        self.g = 9.81
        self.rho = 1000.
        self.f0 = 1e-4
        self.beta = 1e-12
        self.dt = self.dx / np.sqrt(self.g*self.H) * 0.1
        self.f = self.f0 + self.beta*self.y

        self.f = np.pad(self.f, 1, mode='edge')
        self.alpha = self.dt * self.f
        self.b = self.alpha**2/4.

        self.c0 = np.sqrt(self.g*self.H)
        self.cR = self.beta / (self.f/self.c0)**2
        self.tR = self.Lx / self.cR.mean()
        self.RR = self.c0/self.f0

        self.tauy = 0.
        self.taux = -np.cos(np.pi/self.Ly * self.y) * 0.5
        self.eta[:,:] = 0.

        k = 1./(24.*3600.)
        self.kappa = np.zeros(self.x.shape) + k
        self.wbcthickness = self.kappa.mean()/self.beta

    def simulate(self, steps, quiver=False):
        self.df.setncattr('steps', steps)
        output = self.output
        self.m = self.eta.sum()
        for i in tqdm.tqdm(range(steps)):
            if i == 20000:
                self.taux[:,:] = 0.
            self.do_step()
            if i%output == 0:
                self.checkpoint(i//output)
        self.check_mass_conservation()
        self.df.close()


    def step_velocities(self):
        u, v = self.u, self.v
        uh, vh = u, v
        uo, vo = u, v
        eta = self.eta
        # step velocities
        txg = self.dt/self.dx * self.g
        tyg = self.dt/self.dy * self.g
        self.pad_edge(eta)
        # predict
        uh[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1]) + self.dt*self.taux/(self.H * self.rho)
        vh[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1]) + self.dt*self.tauy/(self.H * self.rho)
        uh[1:-1, 1:-1] /= 1 + self.dt * self.kappa
        vh[1:-1, 1:-1] /= 1 + self.dt * self.kappa

        u[:, :] = (uh - self.b*uo + self.alpha*vo)/(1 + self.b)
        v[:, :] = (vh - self.b*vo - self.alpha*uo)/(1 + self.b)
        u[:, -2] = 0.
        v[-2, :] = 0.


class AbyssalSW(BetaPlaneSW):
    def __init__(self, dx, dy, Lx, Ly, quiver=False):
        super(AbyssalSW, self).__init__(dx, dy, Lx, Ly, quiver=quiver)
        self.H = 2000.
        self.g = 0.1
        self.rho = 1000.
        self.f0 = 1e-5
        self.beta = 1e-12
        k = 1e-6
        wup = 1e-4

        self.dt = self.dx / np.sqrt(self.g*self.H) * 0.5
        self.f = self.f0 + self.beta*self.y
        self.f = np.pad(self.f, 1, mode='edge')
        self.alpha = self.dt * self.f
        self.b = self.alpha**2/4.
        self.kappa = np.zeros(self.x.shape) + k
        self.wup = np.zeros(self.x.shape)
        width = 10
        self.wup[-width:,-width:] = wup
        #self.wup[:width,-width:] = -wup
        #self.wup[:,:] = wup
        self.eta[:,:] = 0.

    def step_eta(self):
        u, v, eta = self.u, self.v, self.eta
        h = eta + self.H
        ujk = u[1:-1, 1:-1]; u1k = u[1:-1, :-2]
        vjk = v[1:-1, 1:-1]; v1k = v[:-2, 1:-1]
        fe = np.where(ujk>0, ujk*h[1:-1, 1:-1], ujk*h[1:-1, 2:])
        fw = np.where(u1k>0, u1k*h[1:-1, :-2], u1k*h[1:-1, 1:-1])
        fn = np.where(vjk>0, vjk*h[1:-1, 1:-1], vjk*h[2:, 1:-1])
        fs = np.where(v1k>0, v1k*h[:-2, 1:-1], v1k*h[1:-1, 1:-1])
        # step eta
        eta[1:-1, 1:-1] -= self.dt/self.dx * (fe - fw) + self.dt/self.dy * (fn - fs)
        eta[1:-1, 1:-1] += self.dt * self.wup

    def step_velocities(self):
        u, v = self.u, self.v
        uh, vh = u, v
        uo, vo = u, v
        eta = self.eta
        # step velocities
        txg = self.dt/self.dx * self.g
        tyg = self.dt/self.dy * self.g
        self.pad_edge(eta)
        # predict
        uh[1:-1, 1:-1] -= txg * (eta[1:-1, 2:] - eta[1:-1, 1:-1])
        vh[1:-1, 1:-1] -= txg * (eta[2:, 1:-1] - eta[1:-1, 1:-1])
        uh[1:-1, 1:-1] /= 1 + self.dt * self.kappa
        vh[1:-1, 1:-1] /= 1 + self.dt * self.kappa

        u[:, :] = (uh - self.b*uo + self.alpha*vo)/(1 + self.b)
        v[:, :] = (vh - self.b*vo - self.alpha*uo)/(1 + self.b)
        u[:, -2] = 0.
        v[-2, :] = 0.

