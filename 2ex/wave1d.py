# helpful website
# http://www-paoc.mit.edu/12.950_modeling/notes/dispersion.pdf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation, rc
from IPython.display import HTML

class Wave(object):
    def __init__(self, gpts, width):
        self.gpts = gpts
        self.width = width
        self.g = 9.81
        self.H = 1.
        L = width
        self.x = np.linspace(0., width, gpts)
        self.dx = self.x[1] - self.x[0]

        self.vel = np.zeros(gpts, dtype=np.float)
        # self.vel = np.pad(self.vel, 1, mode='wrap')
        self.eta = np.sin(2*np.pi*self.x/L)
        # self.eta = np.pad(self.eta, 1, mode='wrap')

    def do_step(self, eta, vel, dt):
        g = self.g
        H = self.H
        dx = self.dx

        vel[0] += dt * ( -g * (eta[1] - eta[-1]) / (2 * dx))
        eta[0] += dt * ( -H * (vel[1] - vel[-1]) / (2 * dx))
        vel[-1] += dt * ( -g * (eta[0] - eta[-2]) / (2 * dx))
        eta[-1] += dt * ( -H * (vel[0] - vel[-2]) / (2 * dx))

        for j in range(1, self.gpts-1):
            vel[j] += dt * ( -g * (eta[j+1] - eta[j-1]) / (2 * dx))
            eta[j] += dt * ( -H * (vel[j+1] - vel[j-1]) / (2 * dx))
        return eta, vel

    # def do_step(self, eta, vel, dt):
    #     g = self.g
    #     H = self.H
    #     dx = self.dx

    #     vel[1:-1] += dt * ( -g * (eta[2:] - eta[:-2])/(2*dx) )
    #     eta[1:-1] += dt * ( -g * (vel[2:] - vel[:-2])/(2*dx) )
    #     return vel, eta

    def smoothing(self):
        epsilon = 0.5
        self.eta = (1-epsilon) * self.eta + epsilon/2.*(np.roll(self.eta, 1) + np.roll(self.eta, -1))

    def simulate(self, steps, dt):
        gpts = self.gpts
        eta = self.eta
        vel = self.vel

        ETA = np.zeros((steps, gpts))
        VEL = np.zeros((steps, gpts))

        for i in range(steps):
            eta, vel = self.do_step(eta, vel, dt)
            self.smoothing()
            ETA[i] = eta# [1:-1]
            VEL[i] = vel# [1:-1]
        return ETA, VEL


class StaggeredWave(object):
    def __init__(self, gpts, width):
        self.gpts = gpts
        self.width = width
        self.g = 9.81
        self.H = 1.
        self.L = width

        self.x = np.linspace(0., width, gpts)
        self.dx = self.x[1] - self.x[0]

        self.vel = np.zeros(gpts, dtype=np.float)
        self.eta = np.sin(2*np.pi*self.x/self.L)

    def do_step(self, dt):
        g = self.g
        H = self.H
        dx = self.dx
        vel, eta = self.vel, self.eta

        vel[0] += dt * ( -g * (eta[1] - eta[0]) / (dx))
        eta[0] += dt * ( -H * (vel[0] - vel[-1]) / (dx))
        vel[-1] += dt * ( -g * (eta[0] - eta[-1]) / (dx))
        eta[-1] += dt * ( -H * (vel[-1] - vel[-2]) / (dx))

        for j in range(1, self.gpts-1):
            vel[j] += dt * ( -g * (eta[j+1] - eta[j]) / (dx))
            eta[j] += dt * ( -H * (vel[j] - vel[j-1]) / (dx))
        return eta, vel

    def simulate(self, steps, dt):
        gpts = self.gpts
        eta = self.eta
        vel = self.vel

        ETA = np.zeros((steps, gpts))
        VEL = np.zeros((steps, gpts))

        for i in range(steps):
            eta, vel = self.do_step(dt)
            ETA[i] = eta# [1:-1]
            VEL[i] = vel# [1:-1]
        return ETA, VEL

class StaggeredWaveVector(StaggeredWave):
    def __init__(self, gpts, width):
        super(StaggeredWaveVector, self).__init__(gpts, width)
        self.vel = np.pad(self.vel, 1, mode='wrap')
        self.eta = np.pad(self.eta, 1, mode='wrap')

    def do_step(self, dt):
        g, H, dx = self.g, self.H, self.dx
        vel, eta = self.vel, self.eta

        vel[1:-1] += dt * (-g * (eta[2:]   - eta[1:-1])/dx)
        eta[1:-1] += dt * (-H * (vel[1:-1] - vel[:-2])/dx)
        vel[1] = vel[-2]
        eta[1] = eta[-2]
        return eta, vel

    def simulate(self, steps, dt):
        gpts, eta, vel = self.gpts, self.eta, self.vel
        ETA = np.zeros((steps, gpts))
        VEL = np.zeros((steps, gpts))

        for i in range(steps):
            eta, vel = self.do_step(dt)
            ETA[i] = eta[1:-1]
            VEL[i] = vel[1:-1]
        return ETA, VEL
