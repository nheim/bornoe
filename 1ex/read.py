#!/usr/bin/python
import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt

with Dataset('t0n23w_5day_2byte.cdf') as df:
    lat = df.variables["lat"][:]
    lon = df.variables["lon"][:]
    time = df.variables["time"][:]
    depth = df.variables["depth"][:]
    T_20 = df.variables["temp"][:]

i = -3
temp = T_20[:, i, 0, 0]


from fourier import fourier_transform
n, ft = fourier_transform(temp)
freq = n/time[-1]

N = len(time)
fft = np.fft.fft(temp)[:N/2]
fftfreq = np.fft.fftfreq(time.shape[0])[:N/2]

fig, ax = plt.subplots(1, 2)
ax[0].plot(time, temp)
ax[1].plot(freq, ft)
ax[1].plot(fft.real)
plt.show()


#for i in range(T_20.shape[1]):
#    plt.plot(T_20[:, i, 0, 0], label=depth[i])
#    plt.legend()
#plt.show()


# from netcdfstream.stream import NetcdfStream
# stream = open('t0n23w_5day_2byte.cdf', 'rb')
# nc = NetcdfStream(stream)
# print(nc.version)
# for i in range(3):
#     name, vattrs, data = nc.read_dataset()
#
# plt.plot(data)
# plt.plot(depth, 'o')
# plt.title(name)
# plt.show()
