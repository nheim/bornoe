\section*{\hfil Yanai Waves \hfil }
\label{sec:yanai_waves}
From the shallow water equations one can derive the possible wave forms at the
equator, including their corresponding dispersion relations.
The equatorial regions of the Earths oceans exhibit different dynamical phenomena
than in the rest of the world due to the vanishing Coriolis effect at the equator.
This can be modelled by a $\beta$-plane with $f_0 = 0$
\begin{equation}
  f = f_0 + \beta y = \beta y
\end{equation}
and results in the shallow water equations of the following form:
\begin{align}
  \label{eq:shallow_water}
  \frac{\partial u}{\partial t}& - \beta y v = - g \frac{\partial \eta}{\partial x} \\
  \frac{\partial v}{\partial t}& + \beta y u = - g \frac{\partial \eta}{\partial y} \\ 
  \frac{\partial \eta}{\partial t}& + H \frac{\partial u}{\partial x} +  H \frac{\partial v}{\partial y} = 0
\end{align}
The variables $u$ and $v$ represent zonal and meridional velocities, $\eta$
is the sea surface height, $H$ the depth of the considered ocean basin and 
$g$ the gravitational acceleration.
In this paper we will consider the baroclinic case, where $g$ is the reduced gravity
$g = 0.01$ m/s$^2$ at the depth of the thermocline $H = 400$ m.

Assuming that there is no meridional velocity at the equator, we can drop the $v$-terms
and the first two equations result in:
\begin{equation}
  \frac{\partial^2 \eta}{\partial t^2} - c^2 \frac{\partial ^2 \eta}{\partial x^2} = 0
\end{equation}
with $c = \sqrt{g H}$, resulting in a dispersion relation of $\omega = ck$.
This can be solved by a left-going and a right-going solution:
\begin{equation}
  \eta(x,y,t) = F_l(x+c t, y) + F_r(x - c t, y)
\end{equation}
And with the third shallow water equation we get an expression for $u$:
\begin{equation}
  u = - \frac{g}{c} (F_l - F_r)
\end{equation}
With these two expressions we can find the $y$-dependance of our waves from
the second shallow water equation:
\begin{equation}
  \frac{\partial F_l}{\partial y} = \frac{\beta y}{c} F_l \text{, }
  \frac{\partial F_r}{\partial y} = -\frac{\beta y}{c} F_r
\end{equation}
Only $F_r$ is a physically valid solution because the exponential of $F_l$
grows to infinity with increasing $y$.
Solutions of $F_r$ can be written like:
\begin{align}
  F_r = F(x - c t) \exp{\frac{- \beta y^2}{2c^2}}
\end{align} 
Which describes a non-dispersive wave that is decaying into both hemispheres
and travelling only eastward, very similar to normal Kelvin waves.

With some more difficult considerations one can derive the general
wave solutions of the three variables $u$, $v$, $\eta$ and 
the disperison relation of waves at the equator.
These solutions are based on Hermite polynomials $H_n$ and are all equatorially trapped
like the Kelvin wave.
\begin{equation}
  v_n = H_n(y) F(x - ct) \cdot \exp{\frac{- \beta y^2}{2c^2}}
\end{equation}
If the solution of $v$ for a certain mode $n$ is symmetric,
the two other variables are antisymmetric around the equator.
The right side of Figure~\ref{fig:wavevel_dispersionrel} shows the first three
non-dimensional solutions for the meridional velocity $v$.

The general dispersion relation at the equator can be written like this:
\begin{equation}
  \frac{\omega^2}{c^2} - k^2 - \frac{\beta k}{\omega} = \frac{2(n+1) \beta}{c}
\end{equation}
with the frequency $\omega$, phase speed $c$, wave number $k$ and mode number $n$.
For the case of $n=-1$ we arrive at the already known dispersion relation for
Kelvin waves:
\begin{equation}
  \omega = ck.
\end{equation}
For $n > 0$ and $\omega \rightarrow \infty$ one finds the relation for equatorial
Poincare waves
\begin{equation}
  \omega^2 = c^2k^2 + (2n+1) \beta c,
\end{equation}
and for $n>0$ and $\omega \leftarrow 0$ we obtain the Rossby wave dispersion
relation:
\begin{equation}
  \omega = \frac{-\beta k}{k^2 + 2(n+1)\beta / c}
\end{equation}
The case of $n=0$ results in solutions that are called \textbf{Yanai waves}.
Their dispersion relation reads
\begin{equation}
  k = -\frac{\beta}{\omega} + \frac{w}{c},
\end{equation}
which results in eastward propagating gravity waves for $k \rightarrow \infty$ and in
westward propagating Rossby waves for $k \rightarrow - \infty$.
The disperison relations can be seen in the right part of Fig.~\ref{fig:wavevel_dispersionrel}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{wavevel_dispersionrel.eps}
  \caption{Non-dimensional, meridional velocity profile (left) and dispersion
  relations (right) for the first three equatorial modes.}
  \label{fig:wavevel_dispersionrel}
\end{figure}

To excite and detect Yanai waves in our shallow water model, we are now taking
the previous considerations into account.
Yanai waves (Fig.~\ref{fig:wavevel_dispersionrel}, $n=0$) are not symmetric in their meridional
velocity profile, which means that the SSH and zonal velocity profiles have to be
symmetric.
Thus, instead of using an antisymmetric initial condition like a 2D-Gaussian
centered at the equator,
we use a symmetric initial condition in $\eta$ (Fig.~\ref{fig:yanai_init}).
\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\linewidth]{yanai_init.eps}
  \caption{Initial SSH condition to excite Yanai waves.}
  \label{fig:yanai_init}
\end{figure}
This initial condition consits of two 2D-Gaussians with opposite signs and
shifted slightly form the equator.
The Hofmoeller diagram should show a westward propagating wave with
a depression on one side and an elevation on the other side of the equator
with a phase speed of approximately 0.6 m/s at the equator with the simulated values
(all simulation parameters can be found in Table~\ref{tab:simsetup}).

Figures \ref{fig:rossby_hofmoeller} and \ref{fig:hofmoeller} show
two Hofmoeller diagrams each. The left diagrams show SSH over time of
a longitude band above the equator, the right side below the equator.
Fig.~\ref{fig:rossby_hofmoeller} shows a simulation with antisymmetric inital
conditions, while Fig.\ref{fig:hofmoeller} shows the symmetric case.
We can clearly see that the Yanai wave with the expected, symmetric depression and elevation
in the second case,
while the first case shows a normal Rossby wave with two elevations next to the 
equator.

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{rossby_hofmoeller.eps}
  \caption{Hofmoeller diagram of the antisymmetric starting conditions resulting
  in a normal Rossby wave. In addition to Rossby waves that propagate westward,
  we can see westward propagating Poincare waves. At the first few waves we can
  nicely see the dispersive nature of the Poincare waves as they fade out.
  The ones that make it to the western boundary are reflected as non-dispersive
  Kelvin waves. The phase speed of slightly less than the expected 0.6 m/s can be explained
  by the recording position that is not exactly at the equator, where Rossby waves
  are fastest.}
  \label{fig:rossby_hofmoeller}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{hofmoeller.eps}
  \caption{Hofmoeller diagram of the symmetric initial conditions showing a
  Yanai wave.}
  \label{fig:hofmoeller}
\end{figure}


