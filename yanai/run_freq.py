from shallow_water import create_anim
from rotate_sw import BottomFrictionSW as SW
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from netCDF4 import Dataset
import tqdm


Lx = 4e6
Ly = 4e6
dx = Lx/200.
dy = Lx/200.
steps = 2000
ival = 20

sw = SW(dx, dy, Lx, Ly, quiver=False)
width = 2e5
sw.eta[1:-1, 1:-1] = sw.set_gaussian(Lx/2., 0, width)
sw.print_info()
raise
#sw.simulate(steps)

print 'loading...'
with Dataset(sw.fname, "r") as df:
#with Dataset("centralblob_0.1dt_2e6L.nc", "r") as df:
    ETA = df.variables['eta'][:,:,:]
    x = df.variables['x'][:]
    y = df.variables['y'][:]
print 'done loading...'


ETA = ETA[::10,:,:]
# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.gca()
ax.set_xlim((x.min(), x.max()))
ax.set_ylim((y.min(), y.max()))
im = ax.pcolor(x, y, ETA[0], animated=True, vmin=-0.1, vmax=0.1)
p1, = ax.plot(x[(50,150,150,50),(50,50,150,150)], y[(50,150,150,50),(50,50,150,150)],'o', color='black')
plt.colorbar(im)
pbar = tqdm.tqdm(total=ETA.shape[0])
# animation function. This is called sequentially
def animate(i):
    pbar.update(1)
    im.set_array(ETA[i, :-1, :-1].flatten())
    p1.set_data(x[(50,150,150,50),(50,50,150,150)], y[(50,150,150,50),(50,50,150,150)])
    return im, p1
# call the animator. blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, frames=ETA.shape[0], interval=ival, blit=True)
plt.show()
