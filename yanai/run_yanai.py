from shallow_water import create_anim
from rotate_sw import BFricEQSW as SW
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from netCDF4 import Dataset
import tqdm


Lx = 6000e3
Ly = 6000e3
dx = Lx/200.
dy = Ly/200.
steps = 5000
ival = 20

sw = SW(dx, dy, Lx, Ly)
width = 5e5
#sw.eta[1:-1, 1:-1] = sw.set_gaussian(4*Lx/5., width, width)
#sw.eta[1:-1, 1:-1] += -sw.set_gaussian(4*Lx/5., -width, width)
sw.eta[1:-1, 1:-1] = sw.set_gaussian(4*Lx/5., 0, width)
#sw.simulate(steps)
sw.print_info()
raise

print 'loading...'
with Dataset(sw.fname, "r") as df:
#with Dataset("rossby_poincare.nc", "r") as df:
    ETA = df.variables['eta'][:,:,:]
    x = df.variables['x'][:]
    y = df.variables['y'][:]
    print df.getncattr('simulated_timestep')
    print df.getncattr('saved_timestep')
print 'done loading...'
print ETA.shape

i = 5
#hm = ETA[::i, 100, :]
#time = np.arange(0., sw.dt*steps, sw.dt*i)
#time, xcoord = np.meshgrid(time, sw.x[0])
#print time.shape, xcoord.shape, hm.shape
#plt.pcolormesh(xcoord, time, hm.T)
#plt.show()


ETA = ETA[::50,:,:]

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.gca()
ax.set_xlim((x.min(), x.max()))
ax.set_ylim((y.min(), y.max()))
im = ax.pcolor(x, y, ETA[0], animated=True, vmin=-0.1, vmax=0.1)
plt.colorbar(im)
pbar = tqdm.tqdm(total=ETA.shape[0])
# animation function. This is called sequentially
def animate(i):
    pbar.update(1)
    im.set_array(ETA[i, :-1, :-1].flatten())
    return im,
# call the animator. blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, frames=ETA.shape[0], interval=ival, blit=True)
plt.show()
