from netCDF4 import Dataset


with Dataset("SW_L3000000.0_dx10000.0.nc", "r") as df:
    eta = df.variables['eta'][::2]
