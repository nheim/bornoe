from shallow_water import create_anim
from rotate_sw import WindStressSW as SW
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from netCDF4 import Dataset
import tqdm


Lx = 6e6
Ly = 6e6
dx = 6e4
dy = 6e4
steps = 20000
ival = 20

sw = SW(dx, dy, Lx, Ly)
sw.simulate(steps)
scale = sw.eta.max()
scale = 0.01

print 'loading...'
with Dataset(sw.fname, "r") as df:
#with Dataset("BFricEQSW_L10000000.0_dx50000.0.nc", "r") as df:
    ETA = df.variables['eta'][:,:,:]
    x = df.variables['x'][:]
    y = df.variables['y'][:]
print 'done loading...'
print ETA.shape

i = 5
#hm = ETA[::i, 100, :]
#time = np.arange(0., sw.dt*steps, sw.dt*i)
#time, xcoord = np.meshgrid(time, sw.x[0])
#print time.shape, xcoord.shape, hm.shape
#plt.pcolormesh(xcoord, time, hm.T)
#plt.show()


ETA = ETA[::50,:,:]

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.gca()

def animate(i):
    ax.clear()
    ax.contour(x, y, ETA[i], 20)
    return ax,

ax.contour(x, y, ETA[0])
ani = animation.FuncAnimation(fig, animate, ETA.shape[0], interval=20, blit=True)

plt.show()





#ax.set_xlim((x.min(), x.max()))
#ax.set_ylim((y.min(), y.max()))
#im = ax.pcolor(x, y, ETA[0], animated=True, vmin=-scale, vmax=scale)
#plt.colorbar(im)
#pbar = tqdm.tqdm(total=ETA.shape[0])
## animation function. This is called sequentially
#def animate(i):
#    pbar.update(1)
#    im.set_array(ETA[i, :-1, :-1].flatten())
#    return im,
## call the animator. blit=True means only re-draw the parts that have changed.
#anim = animation.FuncAnimation(fig, animate, frames=ETA.shape[0], interval=ival, blit=True)
#plt.show()
